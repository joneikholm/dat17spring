package com.eikholm.dat17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dat17Application {

	public static void main(String[] args) {
		SpringApplication.run(Dat17Application.class, args);
	}
}
