package com.eikholm.dat17;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController
{
    List<User> users = new ArrayList<>();

    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public String index(){
        System.out.println("received get request to index()");
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute User user, Model model){

        model.addAttribute("users", users);
        model.addAttribute("user", user);
        users.add(user);
        System.out.println("received post request");
        return "login";
    }

    @RequestMapping(value = "/page2", method = RequestMethod.GET)
    public String page2(){
        System.out.println("received get request to page2()");
        return "page3";
    }
}
