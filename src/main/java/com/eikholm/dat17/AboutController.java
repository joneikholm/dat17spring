package com.eikholm.dat17;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AboutController
{
    @RequestMapping(value = "/about.html", method = RequestMethod.GET)
    public String index(){
        System.out.println("received get request to index()");
        return "about";
    }


}
